import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

import java.util.Base64;

public class Example {

    public static void main(String[] args) {
        byte[] key = Base64.getDecoder().decode("TAP5bM97Yg/FEW5yJwz3hGoxlZ2nn09XmJVojeOOhUc=");

        String createJwt = Jwts.builder()
                .claim("userId", 123)
                .signWith(Keys.hmacShaKeyFor(key))
                .compact();

        assert Jwts.parserBuilder()
                .setSigningKey(key)
                .build().
                parseClaimsJws(createJwt)
                .getBody()
                .getSubject()
                .equals("userId");


        System.out.println(createJwt);



//
//        System.out.println(decode);
//
//        System.out.println("userId: " + decode.getBody().get("userId", Integer.class));
    }
}
